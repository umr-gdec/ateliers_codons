---
title: "Installer R et RStudio"
format: revealjs
editor: visual
---

## Télécharger R (1/3)

<br>

Site du [**CRAN (Comprehensive R Archive Network)**](https://cran.r-project.org/index.html).

::: notes
Ce site vous permettra de télécharger la dernière version de R pour votre système d'exploitation (Windows, Mac ou Linux).

Vous pouvez également télécharger les versions précédentes de R grâce à l'archive du site.

Sur la page d'accueil du CRAN, sélectionnez votre système d'exploitation.

Nous allons voir comment télécharger et installer R sur Windows.

Pour les autres systèmes d'exploitation, se référer aux explications données sur le site du CRAN.
:::

![](img/cran_01.png)

## Télécharger R (2/3)

<br>

Après avoir sélectionné **Windows**, cliquez sur **base**:

![](img/cran_02.png)

## Télécharger R (3/3)

<br>

Cliquez sur **Download R-... for Windows**:

![](img/cran_03.png)

<br>

Sélectionnez l'emplacement où sera sauvegardé le fichier d'installation (**Téléchargements** par exemple).

## Installer R (1)

<br>

Naviguez jusqu'à l'emplacement de sauvegarde (*Téléchargements* par exemple) et double-cliquez sur le fichier **R-...-win.exe**

![](img/install_r_01.png)

<br>

Validez les étapes d'installation en laissant les paramètres par défaut.

Une fois l'installation complète, cliquez sur **Terminer**.

<br>

Démarrez R (à l'aide de l'icône sur le bureau par exemple). La fenêtre suivante s'ouvre:

![](img/r_01.png)

<br>

Il s'agit d'une **interface graphique** (Graphical User Interface) permettant de taper du code R.

::: callout-tip
## Environnement de développement intégré

Une large majorité d'utilisateurs de R travaillent à l'aide d'un **environnement de développement intégré** (Integrated Development Environment ou IDE).

Il existe plusieurs IDE permettant de coder en R, les plus connus et utilisés étant **RStudio** et **VSCode**.
:::

<br>

Nous allons voir comment télécharger et installer RStudio.

<br>

# Télécharger et installer RStudio {#download_install_rstudio}

<br>

## Télécharger RStudio

<br>

Pour télécharger RStudio, cliquez sur le lien suivant pour vous rendre sur le site de [**Posit**](https://posit.co/downloads/), la compagnie qui développe, entre autres outils, RStudio (fondée en 2009, cette compagnie initialement appelée RStudio a changé de nom en 2022 pour s'appeler Posit).

Cliquez sur **Download RStudio**:

[![](img/rstudio_01.png){width="75%"}](https://www.rstudio.com/)

<br>

La page suivante vous propose de télécharger R (si vous cliquez sur **Download and Install R** vous arriverez sur la page d'accueil du CRAN).

Comme nous avons déjà installé R, cliquez sur **Download RStudio Desktop for Windows**:

![](img/rstudio_02.png)

<br>

Choisissez l'emplacement de sauvegarde (par exemple **Téléchargements**).

<br>

## Installer RStudio

<br>

Naviguez jusqu'à l'emplacement de sauvegarde (*Téléchargements* par exemple) et double-cliquez sur le fichier **RStudio-...exe**

![](img/rstudio_03.png)

<br>

Validez les étapes d'installation en laissant les paramètres par défaut.

<br>

Une fois RStudio installé, cliquez sur **Fermer**.

<br>

Ouvrez **RStudio**:

![](img/rstudio_04.png)
